import java_cup.runtime.Symbol;

%%
%unicode
%cup
%line
%column

variable=[a-z]
entier=(0|[1-9][0-9]*)
chaine=\"([a-z]|[A-Z])+\"

%%

"=" { return new Symbol(sym.EGAL); }
"(" { return new Symbol(sym.P_O); }
")" { return new Symbol(sym.P_F); }
{chaine} { return new Symbol(sym.CHAINE, yytext()); }
{entier} { return new Symbol(sym.ENTIER, new Integer(yytext())); }
"+" { return new Symbol(sym.ADD); }
"*" { return new Symbol(sym.MULT); }
"$" { return new Symbol(sym.IDENT); }
{variable} { return new Symbol(sym.VARIABLE, yytext()); }
"PRINT" { return new Symbol(sym.PRINT); }
"\n" { return new Symbol(sym.RETOUR_CHARIOT); }
. {}
