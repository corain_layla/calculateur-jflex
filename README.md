# Projet Calculateur Master 1 Compilation

### Environnement:
Java JFlex Cup
Instructions d'installation dans Sujet.pdf

### Utilisation:
Pour compiler, lancer "make" dans le shell
Pour lancer le programme, lancer "make run"

### Notes:
-	expr gère les opérations contenant exclusivement des entiers
-	expr2 gère les opérations sur les chaines.

### à améliorer:
- 	La gestion des erreurs de syntaxe (fichier non reconnu par la grammaire) n'est pas gérée.
- 	Il faut absolument un retour à la ligne en fin de fichier.
-	le fichier d'input est incomplet. Cette grammaire gère tout ce qui est demandé dans le sujet mais tous les exemples ne sont pas présents dans la démo.
-	Pas d'affichage de l'expression calculée
